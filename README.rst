==============================
EBC Traffic Simulation Package
==============================

Development
===========

In order to install the EBC Traffic Simulation Package for development,
do the following:

.. code-block:: bash

    git clone https://gitlab.com/rw1nkler/traffic
    cd traffic
    pip3 install -e .

Now the package is installed in a development mode. Any change in the
package directory will be immidiately reflected when you import it in an
interpreter.
