==========================
City Graph Generator Usage
==========================

Short description
=================

.. code:: bash

    ./city-generator.py -h

    usage: city-generator.py [-h] [--img IMG] OUTPUT

    Create city graph

    positional arguments:
      OUTPUT      Output graph file path

    optional arguments:
      -h, --help  show this help message and exit
      --img IMG   Image file path


Example of use
==============

.. code:: bash

    ./city-generator.py --img graph.png graph.yml

    Give me the name of the city: example1
    Give me the number of crossroads in the city: 6
    Give me the number of city entrances: 6
    Give me a list of crossroads connected to crossroad 1: 2 3
    Give me a list of crossroads connected to crossroad 2: 1 4
    Give me a list of crossroads connected to crossroad 3: 1 4
    Give me a list of crossroads connected to crossroad 4: 2 3 5 6
    Give me a list of crossroads connected to crossroad 5: 4 6
    Give me a list of crossroads connected to crossroad 6: 4
    Give me a list of crossroads connected to entrance 1: 1
    Give me a list of crossroads connected to entrance 2: 3
    Give me a list of crossroads connected to entrance 3: 3
    Give me a list of crossroads connected to entrance 4: 5
    Give me a list of crossroads connected to entrance 5: 6
    Give me a list of crossroads connected to entrance 6: 2
    Give me the length of edge (C1, C2): 200
    Give me the length of edge (C1, C3): 200
    Give me the length of edge (C2, C1): 200
    Give me the length of edge (C2, C4): 200
    Give me the length of edge (C3, C1): 200
    Give me the length of edge (C3, C4): 200
    Give me the length of edge (C4, C2): 200
    Give me the length of edge (C4, C3): 200
    Give me the length of edge (C4, C5): 200
    Give me the length of edge (C4, C6): 300
    Give me the length of edge (C5, C4): 200
    Give me the length of edge (C5, C6): 500
    Give me the length of edge (C6, C4): 300
    Give me the length of edge (En1, C1): 400
    Give me the length of edge (Ex1, C1): 400
    Give me the length of edge (En2, C3): 400
    Give me the length of edge (Ex2, C3): 400
    Give me the length of edge (En3, C3): 600
    Give me the length of edge (Ex3, C3): 600
    Give me the length of edge (En4, C5): 400
    Give me the length of edge (Ex4, C5): 400
    Give me the length of edge (En5, C6): 500
    Give me the length of edge (Ex5, C6): 500
    Give me the length of edge (En6, C2): 800
    Give me the length of edge (Ex6, C2): 800

Example was based on the city from:

.. image:: example1.jpg
    :width: 400px
    :align: center
