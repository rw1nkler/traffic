#!/usr/bin/env python3

from traffic.types import Route

route = Route()
route.read_from_file('route.yml')
route.show()
route.write_to_file('route-internal.yml')
