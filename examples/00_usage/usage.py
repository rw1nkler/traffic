#!/usr/bin/env python3

from traffic.city import Street
from traffic.vehicles import Vehicle
from traffic.types import Route

street = Street(50)
for car_nr in range(10):
    street.add_vehicle(Vehicle(car_nr, car_nr**2, Route()))

street.list_vehicles()
