import enum
import tabulate
import pandas
import pathlib
import sys
import yaml

class RouteFileNotFound(FileNotFoundError):
    pass

class RouteInvalidSymbol(Exception):
    pass

class Route:
    class Direction(enum.Enum):
        LEFT   = enum.auto()
        RIGHT  = enum.auto()
        UP     = enum.auto()
        DOWN   = enum.auto()

    validSymbols = ['L', 'R', 'U', 'D', 'LEFT', 'RIGHT', 'UP', 'DOWN']
    symbolInputConversion = {
        'L'     : Direction.LEFT,
        'R'     : Direction.RIGHT,
        'U'     : Direction.UP,
        'D'     : Direction.DOWN,
        'LEFT'  : Direction.LEFT,
        'RIGHT' : Direction.RIGHT,
        'UP'    : Direction.UP,
        'DOWN'  : Direction.DOWN,
    }
    symbolOutputConversion = {
        Direction.LEFT   : 'LEFT',
        Direction.RIGHT  : 'RIGHT',
        Direction.UP     : 'UP',
        Direction.DOWN   : 'DOWN',
    }

    def __init__(self, route=None):
        self._route = []

        if route != None:
            assert isinstance(route, list), 'Route is not a list with direcion symbols'
            for symbol in route:
                if not self._is_valid_symbol(symbol):
                    raise RouteInvalidSymbol()
                self._route += [self._symbol_to_enum(symbol)]

    def __eq__(self, other):
        return self._route == other._route

    def length(self):
        return len(self._route)

    def clear(self):
        self._route.clear()

    def _is_valid_symbol(self, symbol):
        return True if symbol in Route.validSymbols else False

    def _symbol_to_enum(self, symbol):
        return Route.symbolInputConversion[symbol]

    def _enum_to_symbol(self, enum):
        return Route.symbolOutputConversion[enum]

    def read_from_file(self, path_str):
        path = pathlib.Path(path_str)
        if not path.exists():
            raise RouteFileNotFound()

        with open(path_str, 'r') as stream:
            route_yaml = yaml.load(stream, Loader=yaml.FullLoader)

        route = []
        for symbol in route_yaml['route']:
            if not self._is_valid_symbol(symbol):
                raise RouteInvalidSymbol()
            route += [self._symbol_to_enum(symbol)]
        self._route = route

    def read_from_list(self, route_list):
        assert isinstance(route_list, list), 'route_list should be a python list'
        for symbol in route_list:
            if not self._is_valid_symbol(symbol):
                raise RouteInvalidSymbol()
            self._route += [self._symbol_to_enum(symbol)]

    def write_to_file(self, path_str):
        route = []
        for enum in self._route:
            route += [self._enum_to_symbol(enum)]
        route_yaml = {'route' : route}

        with open(path_str, 'w') as stream:
            yaml.dump(route_yaml, stream=stream)

    def is_empty(self):
        if len(self._route) == 0:
            return True
        else:
            return False

    def show(self):
        direction = []
        for enum in self._route:
            direction += [self._enum_to_symbol(enum)]

        data = {
            'Direction' : direction,
        }

        table = pandas.DataFrame(data)
        print(tabulate.tabulate(table, headers='keys', tablefmt='psql'))

