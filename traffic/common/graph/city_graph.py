from networkx import DiGraph, spring_layout, draw_networkx, write_yaml
from traffic.common.city import Crossroad, Street, Exit, Entrance
import matplotlib.pyplot as plt

class CityGraphGenerator():
    def __init__(self):
        self._city_name = None
        self._number_of_crossroads = None
        self._number_of_entrances = None
        self._number_of_exits = None
        self._list_of_crossroads_connections = []
        self._list_of_entrances_connections = []
        self._list_of_exits_connections = []
        self._graph = None

    def set_city_name(self, name):
        self._city_name = name

    def set_number_of_crossroads(self, no):
        self._number_of_crossroads = no

    def set_number_of_entrances(self, no):
        self._number_of_entrances = no

    def set_number_of_exits(self, no):
        self._number_of_exits = no

    def set_list_of_crossroads_connections(self, conn_list):
        self._list_of_crossroads_connections = conn_list

    def set_list_of_entrances_connections(self, conn_list):
        self._list_of_entrances_connections = conn_list

    def set_list_of_exits_connections(self, conn_list):
        self._list_of_exits_connections = conn_list

    def generate(self):
        assert self._number_of_crossroads != None
        assert self._number_of_entrances != None
        assert self._number_of_exits != None
        assert self._list_of_crossroads_connections != False
        assert self._list_of_entrances_connections != False
        assert self._list_of_exits_connections != False

        crossroads_list = []
        entrances_list = []
        exits_list = []

        self._graph = DiGraph()
        for i in range(self._number_of_crossroads):
            crossroad = Crossroad()

            self._graph.add_node(crossroad)
            crossroads_list += [crossroad]

        for i in range(self._number_of_crossroads):
            for j in self._list_of_crossroads_connections[i]:
                street = Street()
                self._graph.add_edge(crossroads_list[i], crossroads_list[j - 1], object=street)

        for i in range(self._number_of_entrances):
            entrance = Entrance()
            exit = Exit()

            self._graph.add_node(entrance)
            entrances_list += [entrance]

            self._graph.add_node(exit)
            exits_list += [exit]

        for i in range(self._number_of_entrances):
            for j in self._list_of_entrances_connections[i]:
                street = Street()
                self._graph.add_edge(entrances_list[i], crossroads_list[j - 1], object=street)

        for i in range(self._number_of_exits):
            for j in self._list_of_exits_connections[i]:
                street = Street()
                self._graph.add_edge(exits_list[i], crossroads_list[j - 1], object=street)

    def get_graph(self):
        assert self._graph != None
        return self._graph

    def save_img(self, path):
        assert self._graph != None

        pos = spring_layout(self._graph)

        graph_len = len(self._graph.nodes)
        graph_node_list = list(self._graph.nodes)
        color_list = []
        labels_dict = {}
        for i in range(graph_len):
            node_label = graph_node_list[i].get_str_id()
            labels_dict.update({graph_node_list[i]: node_label})
            if isinstance(graph_node_list[i], Entrance):
                color_list += ['r']
            elif isinstance(graph_node_list[i], Exit):
                color_list += ['g']
            else:
                color_list += ['b']

        draw_networkx(self._graph, pos, node_color=color_list, labels=labels_dict)
        plt.savefig(path)

    def save_yaml(self, path):
        assert self._graph != None
        write_yaml(self._graph, path)
