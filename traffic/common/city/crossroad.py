class Crossroad:
    obj_counter = 0

    def __init__(self):
        Crossroad.obj_counter = Crossroad.obj_counter + 1
        self._id = Crossroad.obj_counter

    def get_id(self):
        return self._id

    def get_str_id(self):
       return "C" + str(self._id)

