class Exit:
    obj_counter = 0

    def __init__(self):
        Exit.obj_counter = Exit.obj_counter + 1
        self._id = Exit.obj_counter

    def get_id(self):
        return self._id

    def get_str_id(self):
        return "Ex" + str(self._id)
