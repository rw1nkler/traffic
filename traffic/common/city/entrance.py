class Entrance:
    obj_counter = 0

    def __init__(self):
        Entrance.obj_counter = Entrance.obj_counter + 1
        self._id = Entrance.obj_counter

    def get_id(self):
        return self._id

    def get_str_id(self):
        return "En" + str(self._id)
