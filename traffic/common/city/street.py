class Street:
    obj_counter = 0

    def __init__(self):
        Street.obj_counter = Street.obj_counter + 1
        self._id = Street.obj_counter
        self._length = None

    def get_id(self):
        return self._id

    def get_str_id(self):
        return "St" + str(self._id)

    def set_length(self, length):
        self._length = length

    def get_length(self):
        return self._length
