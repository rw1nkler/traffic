class Vehicle:

    vehicleCounter = 0

    def __init__(self, velocity, space, route):

        Vehicle.vehicleCounter = Vehicle.vehicleCounter + 1

        self._velocity = velocity
        self._space = space
        self._ID = Vehicle.vehicleCounter
        self._street = None
        self._route = route

    def get_ID(self):
        return self._ID

    def get_velocity(self):
        return self._velocity

    def get_space(self):
        return self._space

    def get_route(self):
        return self._route

