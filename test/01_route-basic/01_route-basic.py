#!/usr/bin/env python3

import unittest
from traffic.types import Route, RouteInvalidSymbol

VALID_ROUTE_SYMBOL_LIST = ['LEFT', 'RIGHT', 'DOWN', 'UP', 'L', 'R', 'U', 'D']
INVALID_ROUTE_SYMBOL_LIST = ['', 'right', 'TOP', 'A', '.']

class TestRouteBasic(unittest.TestCase):
    def test__is_valid_symbol(self):
        route = Route()
        for valid_symbol in VALID_ROUTE_SYMBOL_LIST:
            self.assertTrue(route._is_valid_symbol(valid_symbol))

        for invalid_symbol in INVALID_ROUTE_SYMBOL_LIST:
            self.assertFalse(route._is_valid_symbol(invalid_symbol))

    def test__symbol_to_enum(self):
        route = Route()

        self.assertEqual(route._symbol_to_enum('LEFT'),  Route.Direction.LEFT)
        self.assertEqual(route._symbol_to_enum('RIGHT'), Route.Direction.RIGHT)
        self.assertEqual(route._symbol_to_enum('UP'),    Route.Direction.UP)
        self.assertEqual(route._symbol_to_enum('DOWN'),  Route.Direction.DOWN)

        self.assertEqual(route._symbol_to_enum('L'), Route.Direction.LEFT)
        self.assertEqual(route._symbol_to_enum('R'), Route.Direction.RIGHT)
        self.assertEqual(route._symbol_to_enum('U'), Route.Direction.UP)
        self.assertEqual(route._symbol_to_enum('D'), Route.Direction.DOWN)

    def test__enum_to_symbol(self):
        route = Route()

        self.assertEqual(route._enum_to_symbol(Route.Direction.LEFT),  'LEFT')
        self.assertEqual(route._enum_to_symbol(Route.Direction.RIGHT), 'RIGHT')
        self.assertEqual(route._enum_to_symbol(Route.Direction.UP),    'UP')
        self.assertEqual(route._enum_to_symbol(Route.Direction.DOWN),  'DOWN')

    def test_equal(self):
        route = Route()
        route2 = Route()
        self.assertEqual(route, route2)

        route._route += [Route.Direction.LEFT]
        self.assertNotEqual(route, route2)

        route2._route += [Route.Direction.LEFT]
        self.assertEqual(route, route2)

        route._route += [Route.Direction.RIGHT]
        self.assertNotEqual(route, route2)

    def test_length(self):
        route = Route()
        self.assertEqual(route.length(), 0)

    def test_clean(self):
        route = Route(VALID_ROUTE_SYMBOL_LIST)
        route.clear()
        self.assertEqual(len(route._route), 0)

if __name__ == '__main__':
    unittest.main()
