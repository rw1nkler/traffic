#!/usr/bin/env python3

import unittest
from traffic.types import Route, RouteInvalidSymbol, RouteFileNotFound

VALID_ROUTE_SYMBOL_LIST = ['LEFT', 'RIGHT', 'DOWN', 'UP', 'L', 'R', 'U', 'D']
INVALID_ROUTE_SYMBOL_LIST = ['', 'right', 'TOP', 'A' '.']

VALID_FILE_NAME = 'valid-route.yml'
INVALID_FILE_NAME = 'invalid-route.yml'
NON_EXISTING_FILE_NAME = 'non-existing-file.yml'
OUTPUT_FILE_NAME = 'write-test-route.yml'

class TestRoute(unittest.TestCase):
    def test_constructor_empty_route(self):
        route = Route()
        self.assertTrue(route.is_empty())

    def test_constructor_filled_route(self):
        route = Route(VALID_ROUTE_SYMBOL_LIST)
        self.assertEqual(route.length(), len(VALID_ROUTE_SYMBOL_LIST))

    def test_read_from_list(self):
        route = Route()

        route.read_from_list(VALID_ROUTE_SYMBOL_LIST)
        self.assertEqual(route.length(), len(VALID_ROUTE_SYMBOL_LIST))

        route._route.clear()
        with self.assertRaises(RouteInvalidSymbol):
            route.read_from_list(INVALID_ROUTE_SYMBOL_LIST)

    def test_read_from_file(self):
        route = Route()

        with self.assertRaises(RouteFileNotFound):
            route.read_from_file(NON_EXISTING_FILE_NAME)

        route.read_from_file(VALID_FILE_NAME)
        self.assertTrue(route.length() != 0)

        with self.assertRaises(RouteInvalidSymbol):
            route.read_from_file(INVALID_FILE_NAME)

    def test_write_to_file(self):
         route = Route()
         route2 = Route()

         route.read_from_file(VALID_FILE_NAME)
         self.assertTrue(route.length() != 0)

         route.write_to_file(OUTPUT_FILE_NAME)
         route2.read_from_file(OUTPUT_FILE_NAME)

         self.assertEqual(route, route2)

if __name__ == '__main__':
    unittest.main()

