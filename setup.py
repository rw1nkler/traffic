import setuptools

with open('README.rst') as f:
    long_description = f.read()

setuptools.setup(
    name='traffic',
    version='1.0',
    description='EBC Traffic Simulation Package',
    long_description=long_description,
    author='Michal Pogodzinski, Roberto Orozco, Jakub Smus, '
           'Juliusz Tarnowski, Robert Winkler',
    url='https://github.com/rw1nkler/traffic',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
)
