#!/usr/bin/env python3

import sys, argparse
from prompt_toolkit import prompt
from traffic.common.graph import CityGraphGenerator

def parse_arguments():
    parser = argparse.ArgumentParser(description='Create city graph')
    parser.add_argument('output', metavar='OUTPUT',
                    help="Output graph file path")
    parser.add_argument('--img', type=str, help="Image file path", default=None)
    return parser.parse_args()

def collect_basic_data_from_user():
    city_name = prompt("Give me the name of the city: ")

    number_of_crossroads = prompt("Give me the number of crossroads in the city: ")
    try:
        number_of_crossroads = (int(number_of_crossroads))
    except ValueError:
        print("This is not a valid number of crossroads")
        sys.exit()

    number_of_entrances = prompt("Give me the number of city entrances: ")
    try:
        number_of_entrances = (int(number_of_entrances))
    except ValueError:
        print("This is not a valid number of entrances")
        sys.exit()

    list_of_crossroads_connections = []
    for i in range(number_of_crossroads):
        message = "Give me a list of crossroads connected to crossroad %d: " % (i + 1)
        connections = prompt(message)
        try:
            connections_list_str = list(connections.split())
            connections_list_int = [int(x) for x in connections_list_str]
            list_of_crossroads_connections += [connections_list_int]
        except ValueError:
            print("This is not a valid connections list")
            sys.exit()

    list_of_entrances_connections = []
    for i in range(number_of_entrances):
        message = "Give me a list of crossroads connected to entrance %d: " % (i + 1)
        connections = prompt(message)
        try:
            connections_list_str = list(connections.split())
            connections_list_int = [int(x) for x in connections_list_str]
            list_of_entrances_connections += [connections_list_int]
        except ValueError:
            print("This is not a valid entrancess connections list")
            sys.exit()

    result = {}
    result.update({'city_name': city_name})
    result.update({'number_of_crossroads': number_of_crossroads})
    result.update({'number_of_entrances': number_of_entrances})
    result.update({'crossroads_connections': list_of_crossroads_connections})
    result.update({'entrances_connections': list_of_entrances_connections})

    return result

def generate_graph(user_data):
    CGG = CityGraphGenerator()

    CGG.set_city_name(user_data['city_name'])
    CGG.set_number_of_crossroads(user_data['number_of_crossroads'])
    CGG.set_number_of_entrances(user_data['number_of_entrances'])
    CGG.set_number_of_exits(user_data['number_of_entrances'])
    CGG.set_list_of_crossroads_connections(user_data['crossroads_connections'])
    CGG.set_list_of_entrances_connections(user_data['entrances_connections'])
    CGG.set_list_of_exits_connections(user_data['entrances_connections'])

    CGG.generate()
    return CGG

def fill_graph_with_detailed_data_from_user(city_generator):
    edges = list(city_generator.get_graph().edges)
    for i in range(len(edges)):
        edge1 = edges[i][0]
        edge2 = edges[i][1]
        message = "Give me the length of edge (%s, %s): " % (edge1.get_str_id(), edge2.get_str_id())
        length = prompt(message)
        try:
            length = int(length)
        except ValueError:
            print("This is not a valid street length")
            sys.exit()
        street = city_generator.get_graph().edges[edge1, edge2]["object"]
        street.set_length(length)

def save_output_file(city_generator, path):
    city_generator.save_yaml(path)

def save_graph_img(city_generator, path):
    city_generator.save_img(path)

def main():
    args = parse_arguments()
    print(args.img)
    print(args.output)
    user_data = collect_basic_data_from_user()
    city_generator = generate_graph(user_data)
    fill_graph_with_detailed_data_from_user(city_generator)

    save_output_file(city_generator, args.output)
    if args.img != None:
        save_graph_img(city_generator, args.img)

if __name__ == "__main__":
    main()
